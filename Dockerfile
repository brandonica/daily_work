FROM php:7.0.8-apache
 
RUN a2enmod rewrite && docker-php-ext-install mysqli gettext

ENV APP_HOME /var/www/html
 
WORKDIR $APP_HOME
ADD --chown=www-data:www-data ./sendy/ $APP_HOME
ADD ./chown.sh /
ENTRYPOINT ["sh /chown.sh"]

EXPOSE 443
EXPOSE 80
