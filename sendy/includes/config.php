<?php 
	//----------------------------------------------------------------------------------//	
	//                               COMPULSORY SETTINGS
	//----------------------------------------------------------------------------------//
	
	/*  Set the URL to your Sendy installation (without the trailing slash) */
	define('APP_PATH', $_ENV["SENDY_HOST"]);
	
	/*  MySQL database connection credentials (please place values between the apostrophes) */
	$dbHost = $_ENV["MYSQL_HOST"];
	$dbUser = $_ENV["MYSQL_USER"];
	$dbPass = $_ENV["MYSQL_PASSWORD"];
	$dbName = $_ENV["MYSQL_DATABASE"];
	
	
	//----------------------------------------------------------------------------------//	
	//								  OPTIONAL SETTINGS
	//----------------------------------------------------------------------------------//	
	
	/* 
		Change the database character set to something that supports the language you'll
		be using. Example, set this to utf16 if you use Chinese or Vietnamese characters
	*/
	$charset = 'utf8mb4';
	
	/*  Set this if you use a non standard MySQL port.  */
	$dbPort = $_ENV["MYSQL_PORT"];;	
	
	/*  Domain of cookie (99.99% chance you don't need to edit this at all)  */
	define('COOKIE_DOMAIN', '');
	
	//----------------------------------------------------------------------------------//
?>